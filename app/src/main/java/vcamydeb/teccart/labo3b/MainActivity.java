package vcamydeb.teccart.labo3b;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private  int RUN_TIME_CODE = 2 ;
    Button btnShowLocation;
    RadioGroup radioGroup1;
    RadioButton radioButtonGPS;
    RadioButton radioButtonDATA;
    GPSLocationTracker gps;
    int radioResult = 0;
    String radiobut;
    int dataID = 1001;
    int gpsID = 1000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioGroup1 = (RadioGroup)findViewById(R.id.radioGroup1);
        radioButtonGPS = (RadioButton)findViewById(R.id.radioButtonGPS);
        radioButtonDATA = (RadioButton)findViewById(R.id.radioButtonDATA);
        radioButtonGPS.setId(gpsID);
        radioButtonDATA.setId(dataID);
        this.radioButtonGPS.setChecked(true);


        this.radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                choice(radioGroup, checkedId);

            }
        });

        radioResult = this.radioGroup1.getCheckedRadioButtonId();


        final Geocoder geocoder = new Geocoder(this);

        btnShowLocation = (Button) findViewById(R.id.btnLocation);
        btnShowLocation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                gps = new GPSLocationTracker(MainActivity.this, radioResult);
                if (gps.canGetLocation) {
                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();



                    List<Address> adressList = null;
                    try {
                        adressList = geocoder.getFromLocation(latitude, longitude, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    String pays = adressList.get(0).getCountryName();
                    String province = adressList.get(0).getAdminArea();
                    String ville = adressList.get(0).getLocality();

                    Toast.makeText(getApplicationContext(), "Votre position est \nLat: "
                            + latitude + "\nLongitude: " + longitude
                            + " \nPays: " + pays
                            + " \nCode Province: " + province
                            + " \nVille: " + ville, Toast.LENGTH_LONG).show();
                } else {
                    gps.showSettingsAlert();
                }
            }
        });
    }
    private void requestPermissionGPS()
    {
        if(ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)){
            showAlertDialog();
        }
        ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},RUN_TIME_CODE);
    }



    private void showAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Runtime Permission");
        alertDialogBuilder.setMessage("Cette application a besoin d'accéder aux:"+"GPS et  données cellulaire."+"Donner les permissions.").setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }



//    public void onRequestPremissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
//    {
//        if(requestCode == RUN_TIME_CODE)
//        {
//            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
//            {
//                Toast.makeText(this,"Permission accordee, vous pouvez utiliser le GPS", Toast.LENGTH_LONG).show();
//            }
//            else
//            {
//                Toast.makeText(this,"Permission refusée, vous n'avez pas acces au GPS", Toast.LENGTH_LONG).show();
//            }
//        }
//    }


    private boolean isGPSAllowed()
    {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if(result == PackageManager.PERMISSION_GRANTED)
        {
            return true;
        }
        else
            {
            return false;
            }
    }

    private void choice(RadioGroup radioGroup, int checkedId) {
        int checkedRadioId = radioGroup.getCheckedRadioButtonId();

        if(checkedRadioId == gpsID) {
            Toast.makeText(this,"Vous avez selectionné le GPS",Toast.LENGTH_SHORT).show();
            if (isGPSAllowed()) {
//                    Toast.makeText(v.getContext(), "Vous avez deja les permissions pour le GPS", Toast.LENGTH_LONG).show();
                return;
            }
            requestPermissionGPS();

        } else if(checkedRadioId == dataID ) {
            Toast.makeText(this,"Vous avez choisi d'utiliser les données cellulaires",Toast.LENGTH_SHORT).show();
        }
    }
}
